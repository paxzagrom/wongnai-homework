package com.wongnai.interview.movie.search;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import com.wongnai.interview.movie.external.MovieData;
import com.wongnai.interview.movie.external.MoviesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wongnai.interview.movie.Movie;
import com.wongnai.interview.movie.MovieSearchService;
import com.wongnai.interview.movie.external.MovieDataService;

@Component("simpleMovieSearchService")
public class SimpleMovieSearchService implements MovieSearchService {
    @Autowired
    private MovieDataService movieDataService;

    @Override
    public List<Movie> search(String queryText) {
        MoviesResponse movieData = movieDataService.fetchAll();
        List<Movie> movies = movieDataService.convertMovieResponseToMovieModel(movieData);
        String regex = "(\\b" + queryText + "\\b.)";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        List<Movie> searchQuery = movies.stream().filter(movie -> (pattern.matcher(movie.getName()).find())).collect(Collectors.toList());
        return searchQuery;
        //TODO: Step 2 => Implement this method by using data from MovieDataService
        // All test in SimpleMovieSearchServiceIntegrationTest must pass.
        // Please do not change @Component annotation on this class
    }
}
