package com.wongnai.interview.movie.external;

import com.wongnai.interview.movie.Movie;

import java.util.List;

public interface MovieDataService {
	MoviesResponse fetchAll();

	List<Movie> convertMovieResponseToMovieModel(MoviesResponse movieData);
}
