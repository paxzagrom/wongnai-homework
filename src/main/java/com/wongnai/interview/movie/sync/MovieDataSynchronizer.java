package com.wongnai.interview.movie.sync;

import javax.transaction.Transactional;

import com.wongnai.interview.movie.Movie;
import com.wongnai.interview.movie.external.MoviesResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.wongnai.interview.movie.MovieRepository;
import com.wongnai.interview.movie.external.MovieDataService;

import java.util.*;

@Component
public class MovieDataSynchronizer {
    @Autowired
    private MovieDataService movieDataService;

    @Autowired
    private MovieRepository movieRepository;

    private Map<String, List<Long>> invertedTable = new HashMap<>();

    @Transactional

    public void forceSync() {
        //TODO: implement this to sync movie into repository
        MoviesResponse movieData = movieDataService.fetchAll();
        List<Movie> movies = movieDataService.convertMovieResponseToMovieModel(movieData);
        List<Movie> savedMovie = (List<Movie>) movieRepository.saveAll(movies);
        invertedTable = createInvertedIndex(savedMovie);
    }


    public Map<String, List<Long>> getInvertedTable() {
        return invertedTable;
    }

    public Map<String, List<Long>> createInvertedIndex(List<Movie> movies) {
         Map<String, List<Long>> invertedOut = new HashMap<>();
        //loop the map
        //if there is index add the movie id
        //at the end if there are no index create the index by make it lower a case and add the movie id
        invertedOut.put("z", Arrays.asList(0l));
        for (Movie movie : movies) {
            String[] words = movie.getName().split("\\W+");
            for (String each : words) {
                boolean isModify = false;
                for (Map.Entry<String, List<Long>> entry : invertedOut.entrySet()) {
                    if (each.equalsIgnoreCase(entry.getKey())) {
                        //does it set value?
                        List<Long> value = entry.getValue();
                        ArrayList<Long> newValue = new ArrayList<>(value);
                        newValue.add(movie.getId());
                        invertedOut.put(each.toLowerCase(), newValue);
                        isModify = true;
                        break;
                    }
                }
                if (isModify == false) {
                    invertedOut.put(each.toLowerCase(), Arrays.asList(movie.getId()));
                }
            }
        }
        return invertedOut;
    }

}
