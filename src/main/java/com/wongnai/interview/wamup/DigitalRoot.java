package com.wongnai.interview.wamup;


import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.stream.Collectors;

public class DigitalRoot {
	public Object check(long number) {
		if (number < 0) {
			throw new InputMismatchException("invalid InputR");
		}
		while (number>9){
		 	long sum=0;
			List<Long> arrNum = convertNumberToArray(number);
			 for (long num : arrNum) {
				 sum += num;
			 }
			number = sum;
		}
		return number;
	}

	private List<Long> convertNumberToArray(long num) {
		List<Long> arrNum = new ArrayList<>();
		for (; num != 0; num /= 10) {
			arrNum.add(num % 10);
		}
		return arrNum;
	}

}
